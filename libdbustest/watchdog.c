#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "watchdog.h"

typedef struct {
	GPid watchdog;
} DbusTestWatchdogPrivate;

static void dbus_test_watchdog_class_init (DbusTestWatchdogClass *klass);
static void dbus_test_watchdog_init       (DbusTestWatchdog *self);
static void dbus_test_watchdog_finalize   (GObject *object);

G_DEFINE_TYPE_WITH_PRIVATE (DbusTestWatchdog, dbus_test_watchdog, G_TYPE_OBJECT);

/* Initialize class */
static void
dbus_test_watchdog_class_init (DbusTestWatchdogClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = dbus_test_watchdog_finalize;

	return;
}

/* Initialize instance data */
static void
dbus_test_watchdog_init (G_GNUC_UNUSED DbusTestWatchdog *self)
{
	DbusTestWatchdogPrivate *priv = dbus_test_watchdog_get_instance_private (self);

	priv->watchdog = 0;

	return;
}

/* clean up memory */
static void
dbus_test_watchdog_finalize (GObject *object)
{
	DbusTestWatchdog * watchdog = DBUS_TEST_WATCHDOG(object);
	DbusTestWatchdogPrivate *priv = dbus_test_watchdog_get_instance_private (watchdog);

	if (priv->watchdog != 0) {
		kill(priv->watchdog, SIGTERM);
	}

	G_OBJECT_CLASS (dbus_test_watchdog_parent_class)->finalize (object);
	return;
}

/**
 * dbus_test_watchdog_add_pid:
 * @watchdog: Instance of #DbusTestWatchdog
 * @pid: PID to kill
 *
 * Adds a PID for the watchdog to watch.
 */
void
dbus_test_watchdog_add_pid (DbusTestWatchdog * watchdog, GPid pid)
{
	g_return_if_fail(DBUS_TEST_IS_WATCHDOG(watchdog));
	g_return_if_fail(pid != 0);
	DbusTestWatchdogPrivate *priv = dbus_test_watchdog_get_instance_private (watchdog);
	g_return_if_fail(priv->watchdog == 0);

	/* Setting up argument vector */
	gchar * strpid = g_strdup_printf("%d", pid);
	gchar * argv[3];
	argv[0] = WATCHDOG;
	argv[1] = strpid;
	argv[2] = NULL;
	
	GError * error = NULL;

	/* Spawn the watchdog, we now have 60 secs */
	g_spawn_async (NULL, /* cwd */
	               argv,
	               NULL, /* env */
	               0, /* flags */
	               NULL, NULL, /* Setup function */
	               &priv->watchdog,
	               &error);

	g_free(strpid);

	if (error != NULL) {
		g_warning("Unable to start watchdog");
		priv->watchdog = 0;
		g_error_free(error);
		return;
	}

	return;
}

/**
 * dbus_test_watchdog_add_pid:
 * @watchdog: Instance of #DbusTestWatchdog
 *
 * Tell the watchdog not to kill.  For now.
 */
void
dbus_test_watchdog_ping (DbusTestWatchdog * watchdog)
{
	g_return_if_fail(DBUS_TEST_IS_WATCHDOG(watchdog));
	DbusTestWatchdogPrivate *priv = dbus_test_watchdog_get_instance_private (watchdog);

	if (priv->watchdog != 0) {
		kill(priv->watchdog, SIGHUP);
	}

	return;
}
